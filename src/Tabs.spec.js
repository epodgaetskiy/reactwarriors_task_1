import React from "react";
import { Tabs } from "./Tabs";
import { shallow } from "enzyme";

describe(`Tabs`, () => {
  test("начальное состояние", () => {
    let component = shallow(<Tabs />);
    let currentTab = component.find(".ac-tab");
    expect(currentTab.text()).toEqual("Tab1");
  });

  describe(`переход вперед`, () => {
    test("c 1 на 2 таб", () => {
      let component = shallow(<Tabs />);
      let button = component.find(".ac-btn-next");
      button.simulate("click");
      let currentTab = component.find(".ac-tab");
      expect(currentTab.text()).toEqual("Tab2");
    });

    test("c 2 на 3 таб", () => {
      let component = shallow(<Tabs />);
      let button = component.find(".ac-btn-next");
      button.simulate("click");
      button.simulate("click");
      let currentTab = component.find(".ac-tab");
      expect(currentTab.text()).toEqual("Tab3");
    });

    test("c 3 на 4 таб", () => {
      let component = shallow(<Tabs />);
      let button = component.find(".ac-btn-next");
      button.simulate("click");
      button.simulate("click");
      button.simulate("click");
      let currentTab = component.find(".ac-tab");
      expect(currentTab.text()).toEqual("Tab4");
    });
  });
});
